extern crate yaml_rust;
use colored::Color;
use directories_next::ProjectDirs;
use std::fs;
use std::path::Path;
use yaml_rust::yaml::Hash;
use yaml_rust::{ScanError, Yaml, YamlLoader};

#[derive(Debug, PartialEq)]
pub struct Config {
    pub left: Option<Vec<PromptElement>>,
    pub right: Option<Vec<PromptElement>>,
    pub pre_left: Option<Vec<PromptElement>>,
}

#[derive(Debug, PartialEq)]
pub enum ContentType {
    Delimiter(String),
    ScopeStart(String),
    ScopeEnd(String),
    GitWtModified,
    GitWtNew,
    GitIxModified,
    GitIxNew,
    GitStaged, // (all ix summed up)
    GitBranch,
    GitBehind,
    GitAhead,
    EnvVar(String),
    VirtualEnv,
    User,
    Host,
    SSHClient,
    Const(String),
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum Colorization {
    Default,
    Specific(colored::Color),
    BGBefore,
    BGAfter,
    FGBefore,
    FGAfter,
}

impl Colorization {
    pub fn is_pending(&self) -> bool {
        self == &Colorization::FGAfter || self == &Colorization::BGAfter
    }
}

#[derive(Debug, PartialEq)]
pub struct PromptElement {
    //    style: colored::Style,
    pub content: ContentType,
    pub fg: Colorization,
    pub bg: Colorization,
    pub pre: String,
    pub post: String,
    pub skip_all_when_zero: bool,
    pub skip_value_when_zero: bool,
    pub skip_value_when_one: bool,
}

impl PromptElement {
    pub fn is_pending(&self) -> bool {
        self.fg.is_pending() || self.bg.is_pending()
    }
}

pub fn get_config() -> Config {
    if let Some(project_dirs) = ProjectDirs::from("gitlab.com", "dlf", "rompt") {
        let config_dir = project_dirs.config_dir();
        let config_file_path = config_dir.join(Path::new("config.yml"));
        match fs::read_to_string(config_file_path) {
            Ok(file_content) => match factor_config_from_yaml(file_content) {
                Ok(config) => config,
                Err(x) => {
                    println!("{:?}", x);
                    fallback::get_fallback_config()
                }
            },
            Err(_) => fallback::get_fallback_config(),
        }
    } else {
        fallback::get_fallback_config()
    }
}

pub mod fallback {
    use super::Colorization;
    use super::ContentType;

    fn get_right_spec() -> Vec<super::PromptElement> {
        let mut prompt_spec: Vec<super::PromptElement> = Vec::new();
        prompt_spec.push(super::PromptElement {
            fg: Colorization::Specific(colored::Color::Green),
            bg: Colorization::Specific(colored::Color::Black),
            pre: "\u{1F13F}".to_string(),
            post: "".to_string(),
            skip_all_when_zero: true,
            skip_value_when_zero: true,
            skip_value_when_one: true,
            content: ContentType::VirtualEnv,
        });
        prompt_spec.push(super::PromptElement {
            fg: Colorization::Specific(colored::Color::Black),
            bg: Colorization::Specific(colored::Color::Blue),
            pre: "↑".to_string(),
            post: "".to_string(),
            skip_all_when_zero: true,
            skip_value_when_zero: true,
            skip_value_when_one: true,
            content: ContentType::GitStaged,
        });
        prompt_spec.push(super::PromptElement {
            fg: Colorization::Specific(colored::Color::White),
            bg: Colorization::Specific(colored::Color::Magenta),
            pre: "?".to_string(),
            post: "".to_string(),
            skip_all_when_zero: true,
            skip_value_when_zero: true,
            skip_value_when_one: true,
            content: ContentType::GitWtNew,
        });
        prompt_spec.push(super::PromptElement {
            fg: Colorization::Specific(colored::Color::White),
            bg: Colorization::Specific(colored::Color::Blue),
            pre: "\u{270E}".to_string(),
            post: "".to_string(),
            skip_all_when_zero: true,
            skip_value_when_zero: true,
            skip_value_when_one: true,
            content: ContentType::GitWtModified,
        });
        prompt_spec.push(super::PromptElement {
            fg: Colorization::Specific(colored::Color::Black),
            bg: Colorization::Specific(colored::Color::Cyan),
            pre: "\u{E0A0}".to_string(),
            post: "".to_string(),
            skip_all_when_zero: true,
            skip_value_when_zero: true,
            skip_value_when_one: true,
            content: ContentType::GitBranch,
        });
        prompt_spec.push(super::PromptElement {
            fg: Colorization::Specific(colored::Color::Black),
            bg: Colorization::Specific(colored::Color::Blue),
            pre: "⇑".to_string(),
            post: "".to_string(),
            skip_all_when_zero: true,
            skip_value_when_zero: true,
            skip_value_when_one: true,
            content: ContentType::GitAhead,
        });
        prompt_spec.push(super::PromptElement {
            fg: Colorization::Specific(colored::Color::Black),
            bg: Colorization::Specific(colored::Color::Blue),
            pre: "⇓".to_string(),
            post: "".to_string(),
            skip_all_when_zero: true,
            skip_value_when_zero: true,
            skip_value_when_one: true,
            content: ContentType::GitBehind,
        });
        prompt_spec
    }

    fn get_left_spec() -> Vec<super::PromptElement> {
        let mut prompt_spec: Vec<super::PromptElement> = Vec::new();
        prompt_spec.push(super::PromptElement {
            fg: Colorization::Specific(colored::Color::Black),
            bg: Colorization::Specific(colored::Color::Yellow),
            pre: "".to_string(),
            post: "".to_string(),
            skip_all_when_zero: true,
            skip_value_when_zero: true,
            skip_value_when_one: true,
            content: ContentType::Const("\\w".to_string()),
        });
        prompt_spec
    }

    pub fn get_fallback_config() -> super::Config {
        super::Config {
            left: Some(get_left_spec()),
            right: Some(get_right_spec()),
            pre_left: None,
        }
    }
}

#[derive(Debug, PartialEq)]
pub enum ConfigErrorKind {
    ConfigurationNotParsable(ScanError),
    SequenceNotAList,
    SequenceElementNotAHash,
    SequenceElementHashNotOfSizeOne,
    FieldNotParsable,
    ConstElementDoenstHaveValue,
    EnvElementDoenstHaveName,
    DelimiterDoesntHaveValue,
    ColorNotParsable,
    ColorNameUnknown(String),
    UnknownAttribute(String),
}

#[derive(Debug)]
struct ConfigError {
    kind: ConfigErrorKind,
}

fn factor_color_from_yaml(yaml: &Yaml) -> Result<Colorization, ConfigError> {
    if yaml == &Yaml::BadValue {
        Ok(Colorization::Default)
    } else {
        Ok(match yaml.as_str() {
            Some(v) => match v {
                "Black" => Colorization::Specific(Color::Black),
                "Red" => Colorization::Specific(Color::Red),
                "Green" => Colorization::Specific(Color::Green),
                "Yellow" => Colorization::Specific(Color::Yellow),
                "Blue" => Colorization::Specific(Color::Blue),
                "Magenta" => Colorization::Specific(Color::Magenta),
                "Cyan" => Colorization::Specific(Color::Cyan),
                "White" => Colorization::Specific(Color::White),
                "BrightBlack" => Colorization::Specific(Color::BrightBlack),
                "BrightRed" => Colorization::Specific(Color::BrightRed),
                "BrightGreen" => Colorization::Specific(Color::BrightGreen),
                "BrightYellow" => Colorization::Specific(Color::BrightYellow),
                "BrightBlue" => Colorization::Specific(Color::BrightBlue),
                "BrightMagenta" => Colorization::Specific(Color::BrightMagenta),
                "BrightCyan" => Colorization::Specific(Color::BrightCyan),
                "BrightWhite" => Colorization::Specific(Color::BrightWhite),
                "Default" => Colorization::Default,
                "FGAfter" => Colorization::FGAfter,
                "FGBefore" => Colorization::FGBefore,
                "BGAfter" => Colorization::BGAfter,
                "BGBefore" => Colorization::BGBefore,
                x => {
                    return Err(ConfigError {
                        kind: ConfigErrorKind::ColorNameUnknown(x.to_string()),
                    })
                }
            },
            None => {
                return Err(ConfigError {
                    kind: ConfigErrorKind::ColorNotParsable,
                })
            }
        })
    }
}

fn factor_single_promt_element_from_yaml(
    key: &str,
    value: &Yaml,
) -> Result<PromptElement, ConfigError> {
    let content = match key.to_lowercase().as_str() {
        "gitmodified" => ContentType::GitWtModified,
        "gitnew" => ContentType::GitWtNew,
        "gitixmodified" => ContentType::GitIxModified,
        "gitixnew" => ContentType::GitIxNew,
        "gitstaged" => ContentType::GitStaged,
        "gitbranch" => ContentType::GitBranch,
        "gitbehind" => ContentType::GitBehind,
        "gitahead" => ContentType::GitAhead,
        "virtualenv" => ContentType::VirtualEnv,
        "const" => ContentType::Const(match value["value"].as_str() {
            Some(v) => v.to_string(),
            None => {
                return Err(ConfigError {
                    kind: ConfigErrorKind::ConstElementDoenstHaveValue,
                })
            }
        }),
        "delimiter" => ContentType::Delimiter(match value["value"].as_str() {
            Some(v) => v.to_string(),
            None => {
                return Err(ConfigError {
                    kind: ConfigErrorKind::DelimiterDoesntHaveValue,
                })
            }
        }),
        "env" => ContentType::EnvVar(match value["name"].as_str() {
            Some(v) => v.to_string(),
            None => {
                return Err(ConfigError {
                    kind: ConfigErrorKind::EnvElementDoenstHaveName,
                })
            }
        }),
        "user" => ContentType::User,
        "host" => ContentType::Host,
        "sshclient" => ContentType::SSHClient,
        x => {
            return Err(ConfigError {
                kind: ConfigErrorKind::UnknownAttribute(x.to_string()),
            })
        }
    };
    let bg = factor_color_from_yaml(&value["bg"])?;
    let fg = factor_color_from_yaml(&value["fg"])?;
    let pre = match value["pre"].as_str() {
        Some(v) => v.to_string(),
        None => String::from(""),
    };
    let post = match value["post"].as_str() {
        Some(v) => v.to_string(),
        None => String::from(""),
    };
    let skip_all_when_zero = match value["skip-when-zero"].as_bool() {
        Some(v) => v,
        None => false,
    };
    let skip_value_when_zero = match value["skip-value-when-zero"].as_bool() {
        Some(v) => v,
        None => false,
    };
    let skip_value_when_one = match value["skip-value-when-one"].as_bool() {
        Some(v) => v,
        None => false,
    };
    Ok(PromptElement {
        bg,
        fg,
        pre,
        post,
        skip_all_when_zero,
        skip_value_when_zero,
        skip_value_when_one,
        content,
    })
}

fn factor_scope_from_yaml(yaml: &Yaml) -> Result<Vec<PromptElement>, ConfigError> {
    let bg = factor_color_from_yaml(&yaml["bg"])?;
    let fg = factor_color_from_yaml(&yaml["fg"])?;
    let start_bg = match yaml["start-bg"] {
        Yaml::BadValue => bg,
        _ => factor_color_from_yaml(&yaml["start-bg"])?,
    };
    let start_fg = match yaml["start-fg"] {
        Yaml::BadValue => fg,
        _ => factor_color_from_yaml(&yaml["start-fg"])?,
    };
    let end_bg = match yaml["end-bg"] {
        Yaml::BadValue => bg,
        _ => factor_color_from_yaml(&yaml["end-bg"])?,
    };
    let end_fg = match yaml["end-fg"] {
        Yaml::BadValue => fg,
        _ => factor_color_from_yaml(&yaml["end-fg"])?,
    };
    let start_value = match yaml["start-value"].as_str() {
        Some(v) => v.to_string(),
        None => String::from(""),
    };
    let end_value = match yaml["end-value"].as_str() {
        Some(v) => v.to_string(),
        None => String::from(""),
    };
    let inner_elements_yaml = &yaml["elements"];
    let mut result: Vec<PromptElement> = Vec::new();
    result.push(PromptElement {
        bg: start_bg,
        fg: start_fg,
        pre: String::from(""),
        post: String::from(""),
        skip_all_when_zero: false,
        skip_value_when_zero: false,
        skip_value_when_one: false,
        content: ContentType::ScopeStart(start_value),
    });
    if inner_elements_yaml != &Yaml::BadValue {
        result.append(&mut factor_sequence_from_yaml(inner_elements_yaml)?);
    };
    result.push(PromptElement {
        bg: end_bg,
        fg: end_fg,
        pre: String::from(""),
        post: String::from(""),
        skip_all_when_zero: false,
        skip_value_when_zero: false,
        skip_value_when_one: false,
        content: ContentType::ScopeEnd(end_value),
    });
    Ok(result)
}

fn factor_sequence_from_yaml(yaml: &Yaml) -> Result<Vec<PromptElement>, ConfigError> {
    let mut result: Vec<PromptElement> = Vec::new();
    let yaml_array = match yaml.as_vec() {
        Some(arr) => arr,
        None => {
            return Err(ConfigError {
                kind: ConfigErrorKind::SequenceNotAList,
            })
        }
    };
    for elem_yaml in yaml_array {
        let outer_hash: &Hash = match elem_yaml.as_hash() {
            Some(v) => v,
            None => {
                return Err(ConfigError {
                    kind: ConfigErrorKind::SequenceElementNotAHash,
                })
            }
        };
        if outer_hash.len() != 1 {
            return Err(ConfigError {
                kind: ConfigErrorKind::SequenceElementHashNotOfSizeOne,
            });
        }
        let key = outer_hash.keys().next().unwrap().as_str().unwrap();
        let value = outer_hash.values().next().unwrap();
        if key.to_lowercase().as_str() == "scope" {
            result.append(&mut factor_scope_from_yaml(value)?);
        } else {
            result.push(factor_single_promt_element_from_yaml(key, value)?);
        }
    }
    Ok(result)
}

fn factor_config_from_yaml(yaml_string: String) -> Result<Config, ConfigError> {
    let yaml_docs = match YamlLoader::load_from_str(&yaml_string) {
        Ok(v) => v,
        Err(x) => {
            return Err(ConfigError {
                kind: ConfigErrorKind::ConfigurationNotParsable(x),
            })
        }
    };
    let yaml = &yaml_docs[0];
    let left = match &yaml["left"] {
        Yaml::BadValue => None,
        value => Some(factor_sequence_from_yaml(&value)?),
    };
    let right = match &yaml["right"] {
        Yaml::BadValue => None,
        value => Some(factor_sequence_from_yaml(&value)?),
    };
    let pre_left = match &yaml["pre-left"] {
        Yaml::BadValue => None,
        value => Some(factor_sequence_from_yaml(&value)?),
    };
    Ok(Config {
        left,
        right,
        pre_left,
    })
}

#[cfg(test)]
mod tests {

    use super::Color;
    use super::Colorization;
    use super::ContentType;

    #[test]
    fn factor_config_with_right_only() {
        let given = "
        right:
            - GitStaged:
        ";
        let expected = super::Config {
            left: None,
            right: Some(vec![super::PromptElement {
                fg: Colorization::Default,
                bg: Colorization::Default,
                pre: "".to_string(),
                post: "".to_string(),
                skip_all_when_zero: false,
                skip_value_when_zero: false,
                skip_value_when_one: false,
                content: ContentType::GitStaged,
            }]),
            pre_left: None,
        };

        assert_eq!(
            super::factor_config_from_yaml(given.to_string()).unwrap(),
            expected
        );
    }

    #[test]
    fn factor_config_with_pre_left_only() {
        let given = "
        pre-left:
            - GitStaged:
        ";
        let expected = super::Config {
            left: None,
            right: None,
            pre_left: Some(vec![super::PromptElement {
                fg: Colorization::Default,
                bg: Colorization::Default,
                pre: "".to_string(),
                post: "".to_string(),
                skip_all_when_zero: false,
                skip_value_when_zero: false,
                skip_value_when_one: false,
                content: ContentType::GitStaged,
            }]),
        };

        assert_eq!(
            super::factor_config_from_yaml(given.to_string()).unwrap(),
            expected
        );
    }

    #[test]
    fn factor_one_element_with_defaults() {
        let given = "
        left:
            - GitStaged:
        ";
        let expected = super::Config {
            left: Some(vec![super::PromptElement {
                fg: Colorization::Default,
                bg: Colorization::Default,
                pre: "".to_string(),
                post: "".to_string(),
                skip_all_when_zero: false,
                skip_value_when_zero: false,
                skip_value_when_one: false,
                content: ContentType::GitStaged,
            }]),
            right: None,
            pre_left: None,
        };

        assert_eq!(
            super::factor_config_from_yaml(given.to_string()).unwrap(),
            expected
        );
    }

    #[test]
    fn factor_one_element_with_all_custom_attribs() {
        let given = "
        left:
            - GitStaged:
                bg: Blue
                fg: Black
                pre: ⇑
                post: ↑
                skip-when-zero: true
                skip-value-when-zero: true
                skip-value-when-one: true
        ";
        let expected = super::Config {
            left: Some(vec![super::PromptElement {
                fg: Colorization::Specific(colored::Color::Black),
                bg: Colorization::Specific(colored::Color::Blue),
                pre: "⇑".to_string(),
                post: "↑".to_string(),
                skip_all_when_zero: true,
                skip_value_when_zero: true,
                skip_value_when_one: true,
                content: ContentType::GitStaged,
            }]),
            right: None,
            pre_left: None,
        };

        assert_eq!(
            super::factor_config_from_yaml(given.to_string()).unwrap(),
            expected
        );
    }

    fn _get_default_element_by_color(bg: Colorization, fg: Colorization) -> super::PromptElement {
        super::PromptElement {
            fg,
            bg,
            pre: String::from(""),
            post: String::from(""),
            skip_all_when_zero: false,
            skip_value_when_zero: false,
            skip_value_when_one: false,
            content: ContentType::GitStaged,
        }
    }

    #[test]
    fn all_base_colors_by_string() {
        let given = "
        left:
            - GitStaged:
                bg: Black
                fg: BrightBlack
            - GitStaged:
                bg: Red
                fg: BrightRed
            - GitStaged:
                bg: Green
                fg: BrightGreen
            - GitStaged:
                bg: Yellow
                fg: BrightYellow
            - GitStaged:
                bg: Blue
                fg: BrightBlue
            - GitStaged:
                bg: Magenta
                fg: BrightMagenta
            - GitStaged:
                bg: Cyan
                fg: BrightCyan
            - GitStaged:
                bg: White
                fg: BrightWhite
            - GitStaged:
        ";
        let expected = super::Config {
            left: Some(vec![
                _get_default_element_by_color(
                    Colorization::Specific(super::Color::Black),
                    Colorization::Specific(super::Color::BrightBlack),
                ),
                _get_default_element_by_color(
                    Colorization::Specific(super::Color::Red),
                    Colorization::Specific(super::Color::BrightRed),
                ),
                _get_default_element_by_color(
                    Colorization::Specific(super::Color::Green),
                    Colorization::Specific(super::Color::BrightGreen),
                ),
                _get_default_element_by_color(
                    Colorization::Specific(super::Color::Yellow),
                    Colorization::Specific(super::Color::BrightYellow),
                ),
                _get_default_element_by_color(
                    Colorization::Specific(super::Color::Blue),
                    Colorization::Specific(super::Color::BrightBlue),
                ),
                _get_default_element_by_color(
                    Colorization::Specific(super::Color::Magenta),
                    Colorization::Specific(super::Color::BrightMagenta),
                ),
                _get_default_element_by_color(
                    Colorization::Specific(super::Color::Cyan),
                    Colorization::Specific(super::Color::BrightCyan),
                ),
                _get_default_element_by_color(
                    Colorization::Specific(super::Color::White),
                    Colorization::Specific(super::Color::BrightWhite),
                ),
                _get_default_element_by_color(Colorization::Default, Colorization::Default),
            ]),
            right: None,
            pre_left: None,
        };

        assert_eq!(
            super::factor_config_from_yaml(given.to_string()).unwrap(),
            expected
        );
    }

    #[test]
    fn all_special_colors_by_string() {
        let given = "
        left:
            - GitStaged:
                bg: Default
                fg: BGAfter
            - GitStaged:
                bg: Default
                fg: BGBefore
            - GitStaged:
                bg: Default
                fg: FGAfter
            - GitStaged:
                bg: Default
                fg: FGBefore
            - GitStaged:
                bg: FGBefore
                fg: Default
        ";
        let expected = super::Config {
            left: Some(vec![
                _get_default_element_by_color(Colorization::Default, Colorization::BGAfter),
                _get_default_element_by_color(Colorization::Default, Colorization::BGBefore),
                _get_default_element_by_color(Colorization::Default, Colorization::FGAfter),
                _get_default_element_by_color(Colorization::Default, Colorization::FGBefore),
                _get_default_element_by_color(Colorization::FGBefore, Colorization::Default),
            ]),
            right: None,
            pre_left: None,
        };

        assert_eq!(
            super::factor_config_from_yaml(given.to_string()).unwrap(),
            expected
        );
    }

    fn _get_default_element_by_type(content: ContentType) -> super::PromptElement {
        super::PromptElement {
            fg: Colorization::Default,
            bg: Colorization::Default,
            pre: String::from(""),
            post: String::from(""),
            skip_all_when_zero: false,
            skip_value_when_zero: false,
            skip_value_when_one: false,
            content,
        }
    }

    fn _get_colorized_element_by_type(
        content: ContentType,
        bg: Colorization,
        fg: Colorization,
    ) -> super::PromptElement {
        super::PromptElement {
            fg,
            bg,
            pre: String::from(""),
            post: String::from(""),
            skip_all_when_zero: false,
            skip_value_when_zero: false,
            skip_value_when_one: false,
            content,
        }
    }

    #[test]
    fn all_content_types() {
        let given = "
        left:
            - GitModified:
            - GitNew:
            - GitIxModified:
            - GitIxNew:
            - GitStaged:
            - GitBranch:
            - GitBehind:
            - GitAhead:
            - VirtualEnv:
            - Const:
                value: moo
            - Delimiter:
                value: 
                fg-first: false
            - Env:
                name: änf
            - Host:
            - User:
            - SSHClient:
        ";
        let expected = super::Config {
            left: Some(vec![
                _get_default_element_by_type(ContentType::GitWtModified),
                _get_default_element_by_type(ContentType::GitWtNew),
                _get_default_element_by_type(ContentType::GitIxModified),
                _get_default_element_by_type(ContentType::GitIxNew),
                _get_default_element_by_type(ContentType::GitStaged),
                _get_default_element_by_type(ContentType::GitBranch),
                _get_default_element_by_type(ContentType::GitBehind),
                _get_default_element_by_type(ContentType::GitAhead),
                _get_default_element_by_type(ContentType::VirtualEnv),
                _get_default_element_by_type(ContentType::Const("moo".to_string())),
                _get_default_element_by_type(ContentType::Delimiter("".to_string())),
                _get_default_element_by_type(ContentType::EnvVar("änf".to_string())),
                _get_default_element_by_type(ContentType::Host),
                _get_default_element_by_type(ContentType::User),
                _get_default_element_by_type(ContentType::SSHClient),
            ]),
            right: None,
            pre_left: None,
        };

        assert_eq!(
            super::factor_config_from_yaml(given.to_string()).unwrap(),
            expected
        );
    }

    #[test]
    fn scope_with_defaults() {
        let given = "
        left:
            - Const:
                value: a
            - Scope:
                elements:
                    - Const:
                        value: b
            - Const:
                value: c
        ";
        let expected = super::Config {
            left: Some(vec![
                _get_default_element_by_type(ContentType::Const("a".to_string())),
                _get_default_element_by_type(ContentType::ScopeStart("".to_string())),
                _get_default_element_by_type(ContentType::Const("b".to_string())),
                _get_default_element_by_type(ContentType::ScopeEnd("".to_string())),
                _get_default_element_by_type(ContentType::Const("c".to_string())),
            ]),
            right: None,
            pre_left: None,
        };

        assert_eq!(
            super::factor_config_from_yaml(given.to_string()).unwrap(),
            expected
        );
    }

    #[test]
    fn scope_with_custom_colors_and_values() {
        let given = "
        left:
            - Const:
                value: a
            - Scope:
                start-value: (
                end-value: )
                start-bg: Black
                start-fg: White
                end-bg: Blue
                end-fg: Yellow
                elements:
                    - Const:
                        value: b
            - Const:
                value: c
        ";
        let expected = super::Config {
            left: Some(vec![
                _get_default_element_by_type(ContentType::Const("a".to_string())),
                super::PromptElement {
                    bg: Colorization::Specific(super::Color::Black),
                    fg: Colorization::Specific(super::Color::White),
                    content: ContentType::ScopeStart(String::from("(")),
                    post: String::from(""),
                    pre: String::from(""),
                    skip_all_when_zero: false,
                    skip_value_when_zero: false,
                    skip_value_when_one: false,
                },
                _get_default_element_by_type(ContentType::Const("b".to_string())),
                super::PromptElement {
                    bg: Colorization::Specific(super::Color::Blue),
                    fg: Colorization::Specific(super::Color::Yellow),
                    content: ContentType::ScopeEnd(String::from(")")),
                    post: String::from(""),
                    pre: String::from(""),
                    skip_all_when_zero: false,
                    skip_value_when_zero: false,
                    skip_value_when_one: false,
                },
                _get_default_element_by_type(ContentType::Const("c".to_string())),
            ]),
            right: None,
            pre_left: None,
        };

        assert_eq!(
            super::factor_config_from_yaml(given.to_string()).unwrap(),
            expected
        );
    }

    #[test]
    fn scopes_nested() {
        let given = "
        left:
            - Const:
                value: a
            - Scope:
                start-value: (
                end-value: )
                elements:
                    - Scope:
                        start-value: '<<'
                        end-value: '>>'
                        elements:
                            - Const:
                                value: b
                    - Const:
                        value: c
            - Const:
                value: d
        ";
        let expected = super::Config {
            left: Some(vec![
                _get_default_element_by_type(ContentType::Const("a".to_string())),
                _get_default_element_by_type(ContentType::ScopeStart("(".to_string())),
                _get_default_element_by_type(ContentType::ScopeStart("<<".to_string())),
                _get_default_element_by_type(ContentType::Const("b".to_string())),
                _get_default_element_by_type(ContentType::ScopeEnd(">>".to_string())),
                _get_default_element_by_type(ContentType::Const("c".to_string())),
                _get_default_element_by_type(ContentType::ScopeEnd(")".to_string())),
                _get_default_element_by_type(ContentType::Const("d".to_string())),
            ]),
            right: None,
            pre_left: None,
        };

        assert_eq!(
            super::factor_config_from_yaml(given.to_string()).unwrap(),
            expected
        );
    }

    #[test]
    fn scope_start_colors_override_standard() {
        let given = "
        left:
            - Scope:
                bg: Black
                fg: White
                start-bg: Red
                start-fg: Green
                start-value: a
                end-value: o
                elements:
                    - Const:
                        value: b
        ";
        let expected = super::Config {
            left: Some(vec![
                _get_colorized_element_by_type(
                    ContentType::ScopeStart("a".to_string()),
                    Colorization::Specific(Color::Red),
                    Colorization::Specific(Color::Green),
                ),
                _get_colorized_element_by_type(
                    ContentType::Const("b".to_string()),
                    Colorization::Default,
                    Colorization::Default,
                ),
                _get_colorized_element_by_type(
                    ContentType::ScopeEnd("o".to_string()),
                    Colorization::Specific(Color::Black),
                    Colorization::Specific(Color::White),
                ),
            ]),
            right: None,
            pre_left: None,
        };

        assert_eq!(
            super::factor_config_from_yaml(given.to_string()).unwrap(),
            expected
        );
    }

    #[test]
    fn scope_end_colors_override_standard() {
        let given = "
        left:
            - Scope:
                bg: Black
                fg: White
                end-bg: Red
                end-fg: Green
                start-value: a
                end-value: o
                elements:
                    - Const:
                        value: b
        ";
        let expected = super::Config {
            left: Some(vec![
                _get_colorized_element_by_type(
                    ContentType::ScopeStart("a".to_string()),
                    Colorization::Specific(Color::Black),
                    Colorization::Specific(Color::White),
                ),
                _get_colorized_element_by_type(
                    ContentType::Const("b".to_string()),
                    Colorization::Default,
                    Colorization::Default,
                ),
                _get_colorized_element_by_type(
                    ContentType::ScopeEnd("o".to_string()),
                    Colorization::Specific(Color::Red),
                    Colorization::Specific(Color::Green),
                ),
            ]),
            right: None,
            pre_left: None,
        };

        assert_eq!(
            super::factor_config_from_yaml(given.to_string()).unwrap(),
            expected
        );
    }
}
