use colored::*;

use crate::config;
use crate::config::Colorization;
use crate::EnvData;
use crate::GitData;

struct Colors {
    bg: Option<Color>,
    fg: Option<Color>,
}

#[cfg(test)]
fn colorize(s: &String, fg: Option<Color>, bg: Option<Color>) -> colored::ColoredString {
    let result = format!(
        "[{}@{}:{}]",
        match fg {
            Some(c) => format!("{:?}", c),
            None => String::from("×"),
        },
        match bg {
            Some(c) => format!("{:?}", c),
            None => String::from("×"),
        },
        s
    );
    colored::ColoredString::from(result.as_str())
}

#[cfg(not(test))]
fn colorize(s: &String, fg: Option<Color>, bg: Option<Color>) -> colored::ColoredString {
    let c = match fg {
        Some(col) => s.color(col),
        None => colored::ColoredString::from(s.as_str()),
    };
    let d = match bg {
        Some(col) => c.on_color(col),
        None => c,
    };
    d
}

fn int_to_string_zero_one_tuple(s: u64) -> ElementEvaluation {
    ElementEvaluation {
        data_string: s.to_string(),
        is_zero: s == 0,
        is_one: s == 1,
    }
}

pub struct Prompt {
    pub string: String,
    pub length: usize,
}

pub struct Element {
    string: String,
    fg: Option<Color>,
    bg: Option<Color>,
    fg_spec: Colorization,
    bg_spec: Colorization,
}

pub struct Delimiter {
    string: String,
    fg_spec: Colorization,
    bg_spec: Colorization,
}

pub struct ScopeStart {
    string: String,
    fg_spec: Colorization,
    bg_spec: Colorization,
}

pub struct PromptString {
    current_delimiter: Option<Delimiter>,
    pending_scope_starts: Vec<ScopeStart>,
    delimiter_on_first_scope_start: Option<Delimiter>,
    elements: Vec<Element>,
    debug_output: bool,
}

struct ElementEvaluation {
    data_string: String,
    is_zero: bool,
    is_one: bool,
}

impl PromptString {
    pub fn new(debug_output: bool) -> PromptString {
        PromptString {
            current_delimiter: None,
            pending_scope_starts: Vec::new(),
            delimiter_on_first_scope_start: None,
            elements: Vec::new(),
            debug_output,
        }
    }

    pub fn factor_from_spec(
        prompt_spec: &Vec<config::PromptElement>,
        debug_output: bool,
        git_data: &Option<GitData>,
        env_data: &EnvData,
    ) -> PromptString {
        let mut prompt = PromptString::new(debug_output);
        for element in prompt_spec {
            if debug_output {
                println!("Applying {:?}", element);
            };
            prompt.apply(&element, git_data, env_data)
        }
        prompt
    }

    fn element_evaluation_from_string_option(option: Option<String>) -> ElementEvaluation {
        match option {
            Some(val) => ElementEvaluation {
                data_string: val.to_string(),
                is_zero: false,
                is_one: false,
            },
            None => ElementEvaluation {
                data_string: String::from(""),
                is_zero: true, // don't care
                is_one: false, // don't care
            },
        }
    }

    fn evaluate_element(
        &mut self,
        element: &config::PromptElement,
        git_data: &Option<GitData>,
        env_data: &EnvData,
    ) -> ElementEvaluation {
        match &element.content {
            config::ContentType::Delimiter(string) => {
                self.current_delimiter = match string.as_str() {
                    "" => None,
                    s => Some(Delimiter {
                        fg_spec: element.fg,
                        bg_spec: element.bg,
                        string: s.to_string(),
                    }),
                };
                ElementEvaluation {
                    data_string: String::from(""),
                    is_zero: true, // don't care
                    is_one: false, // don't care
                }
            }
            config::ContentType::ScopeStart(string) => {
                if self.pending_scope_starts.len() == 0 {
                    self.delimiter_on_first_scope_start = match &self.current_delimiter {
                        None => None,
                        Some(d) => Some(Delimiter {
                            bg_spec: d.bg_spec,
                            fg_spec: d.fg_spec,
                            string: d.string.to_string(),
                        }),
                    };
                };
                self.pending_scope_starts.push(ScopeStart {
                    fg_spec: element.fg,
                    bg_spec: element.bg,
                    string: string.to_string(),
                });
                ElementEvaluation {
                    data_string: String::from(""),
                    is_zero: true, // don't care
                    is_one: false, // don't care
                }
            }
            config::ContentType::ScopeEnd(string) => {
                if self.pending_scope_starts.is_empty() {
                    ElementEvaluation {
                        data_string: String::from(string),
                        is_zero: true,
                        is_one: false,
                    }
                } else {
                    self.pending_scope_starts.pop();
                    ElementEvaluation {
                        data_string: String::from(""),
                        is_zero: true,
                        is_one: false,
                    }
                }
            }
            config::ContentType::GitWtNew => match git_data {
                Some(gd) => int_to_string_zero_one_tuple(gd.status_data.wt_new as u64),
                None => ElementEvaluation {
                    data_string: String::from(""),
                    is_zero: true, // don't care
                    is_one: false, // don't care
                },
            },
            config::ContentType::GitWtModified => match git_data {
                Some(gd) => int_to_string_zero_one_tuple(gd.status_data.wt_modified as u64),
                None => ElementEvaluation {
                    data_string: String::from(""),
                    is_zero: true, // don't care
                    is_one: false, // don't care
                },
            },
            config::ContentType::GitIxNew => match git_data {
                Some(gd) => int_to_string_zero_one_tuple(gd.status_data.ix_new as u64),
                None => ElementEvaluation {
                    data_string: String::from(""),
                    is_zero: true, // don't care
                    is_one: false, // don't care
                },
            },
            config::ContentType::GitIxModified => match git_data {
                Some(gd) => int_to_string_zero_one_tuple(gd.status_data.ix_modified as u64),
                None => ElementEvaluation {
                    data_string: String::from(""),
                    is_zero: true, // don't care
                    is_one: false, // don't care
                },
            },
            config::ContentType::GitStaged => match git_data {
                Some(gd) => int_to_string_zero_one_tuple(
                    (gd.status_data.ix_modified
                        + gd.status_data.ix_new
                        + gd.status_data.ix_deleted
                        + gd.status_data.ix_renamed
                        + gd.status_data.ix_typechange) as u64,
                ),
                None => ElementEvaluation {
                    data_string: String::from(""),
                    is_zero: true, // don't care
                    is_one: false, // don't care
                },
            },
            config::ContentType::GitBranch => match git_data {
                Some(gd) => match &gd.branch_data.branch_name {
                    Some(n) => ElementEvaluation {
                        data_string: n.to_string(),
                        is_zero: false,
                        is_one: false,
                    },
                    None => ElementEvaluation {
                        data_string: String::from(""),
                        is_zero: true, // don't care
                        is_one: false, // don't care
                    },
                },
                None => ElementEvaluation {
                    data_string: String::from(""),
                    is_zero: true, // don't care
                    is_one: false, // don't care
                },
            },
            config::ContentType::GitBehind => match git_data {
                Some(gd) => match &gd.branch_data.ahead_behind {
                    Some(n) => int_to_string_zero_one_tuple(n.1 as u64),
                    None => ElementEvaluation {
                        data_string: String::from(""),
                        is_zero: true, // don't care
                        is_one: false, // don't care
                    },
                },
                None => ElementEvaluation {
                    data_string: String::from(""),
                    is_zero: true, // don't care
                    is_one: false, // don't care
                },
            },
            config::ContentType::GitAhead => match git_data {
                Some(gd) => match &gd.branch_data.ahead_behind {
                    Some(n) => int_to_string_zero_one_tuple(n.0 as u64),
                    None => ElementEvaluation {
                        data_string: String::from(""),
                        is_zero: true, // don't care
                        is_one: false, // don't care
                    },
                },
                None => ElementEvaluation {
                    data_string: String::from(""),
                    is_zero: true, // don't care
                    is_one: false, // don't care
                },
            },
            config::ContentType::VirtualEnv => {
                PromptString::element_evaluation_from_string_option(env_data.get_py_virt_env())
            }
            config::ContentType::Host => {
                PromptString::element_evaluation_from_string_option(env_data.get_host())
            }
            config::ContentType::User => {
                PromptString::element_evaluation_from_string_option(env_data.get_user())
            }
            config::ContentType::SSHClient => {
                PromptString::element_evaluation_from_string_option(env_data.get_ssh_client())
            }
            config::ContentType::EnvVar(name) => match &env_data.get_env_variable(name.to_string())
            {
                Some(val) => ElementEvaluation {
                    data_string: val.to_string(),
                    is_zero: val.len() == 0 || val == "0",
                    is_one: val == "1",
                },
                None => ElementEvaluation {
                    data_string: String::from(""),
                    is_zero: true, // don't care
                    is_one: false, // don't care
                },
            },
            config::ContentType::Const(string) => ElementEvaluation {
                data_string: string.to_string(),
                is_zero: false,
                is_one: false,
            },
        }
    }

    pub fn apply(
        &mut self,
        element: &config::PromptElement,
        git_data: &Option<GitData>,
        env_data: &EnvData,
    ) {
        let element_evaluation = self.evaluate_element(element, git_data, env_data);
        if !element_evaluation.is_zero || !element.skip_all_when_zero {
            let this_elements_string = format!(
                "{}{}{}",
                element.pre,
                if (!element_evaluation.is_zero || !element.skip_value_when_zero)
                    && (!element_evaluation.is_one || !element.skip_value_when_one)
                {
                    element_evaluation.data_string
                } else {
                    "".to_string()
                },
                element.post
            );
            if !this_elements_string.is_empty() {
                if self.debug_output {
                    print!("  → {}", this_elements_string);
                };
                if self.pending_scope_starts.len() > 0 {
                    if let Some(delim) = &self.delimiter_on_first_scope_start {
                        if self.debug_output {
                            print!(" - scope start delimiter: {}", delim.string);
                        };
                        if !self.elements.is_empty() {
                            &self.elements.push(Element {
                                string: delim.string.to_string(),
                                fg_spec: delim.fg_spec,
                                bg_spec: delim.bg_spec,
                                fg: None,
                                bg: None,
                            });
                        };
                    } else if self.debug_output {
                        print!(" - no delimiter");
                    };
                    if self.debug_output {
                        print!(" - append stash");
                    };
                    for scope_start in &self.pending_scope_starts {
                        self.elements.push(Element {
                            string: scope_start.string.to_string(),
                            fg_spec: scope_start.fg_spec,
                            bg_spec: scope_start.bg_spec,
                            fg: None,
                            bg: None,
                        });
                    }
                    self.pending_scope_starts.clear();
                } else if let config::ContentType::ScopeEnd(_) = element.content {
                } else {
                    if let Some(delim) = &self.current_delimiter {
                        if self.debug_output {
                            print!(" - delimiter: {}", delim.string);
                        };
                        if !self.elements.is_empty() {
                            &self.elements.push(Element {
                                string: delim.string.to_string(),
                                fg_spec: delim.fg_spec,
                                bg_spec: delim.bg_spec,
                                fg: None,
                                bg: None,
                            });
                        };
                    } else if self.debug_output {
                        print!(" - no delimiter");
                    };
                };
                let element = Element {
                    string: this_elements_string,
                    fg_spec: element.fg,
                    bg_spec: element.bg,
                    fg: None,
                    bg: None,
                };
                &self.elements.push(element);
            } else if self.debug_output {
                print!("  → Empty");
            }
        } else if self.debug_output {
            print!("  → Skip");
        }
        if self.debug_output {
            println!(
                " - defined delim: {}",
                match &self.current_delimiter {
                    None => "None".to_string(),
                    Some(d) => d.string.to_string(),
                }
            );
        };
    }

    pub fn get_promt(&mut self) -> Prompt {
        let mut length = 0 as usize;
        let mut last_colors = Colors { fg: None, bg: None };
        for e in self.elements.iter_mut().rev() {
            e.fg = match e.fg_spec {
                Colorization::Specific(col) => Some(col),
                Colorization::Default => None,
                Colorization::FGBefore => None,
                Colorization::BGBefore => None,
                Colorization::FGAfter => last_colors.fg,
                Colorization::BGAfter => last_colors.bg,
            };
            e.bg = match e.bg_spec {
                Colorization::Specific(col) => Some(col),
                Colorization::Default => None,
                Colorization::FGBefore => None,
                Colorization::BGBefore => None,
                Colorization::FGAfter => last_colors.fg,
                Colorization::BGAfter => last_colors.bg,
            };
            last_colors = Colors { fg: e.fg, bg: e.bg };
        }
        last_colors = Colors { fg: None, bg: None };
        for e in self.elements.iter_mut() {
            length += e.string.chars().count();
            e.fg = match e.fg_spec {
                Colorization::Specific(_) => e.fg,
                Colorization::Default => None,
                Colorization::FGBefore => last_colors.fg,
                Colorization::BGBefore => last_colors.bg,
                Colorization::FGAfter => e.fg,
                Colorization::BGAfter => e.fg,
            };
            e.bg = match e.bg_spec {
                Colorization::Specific(_) => e.bg,
                Colorization::Default => None,
                Colorization::FGBefore => last_colors.fg,
                Colorization::BGBefore => last_colors.bg,
                Colorization::FGAfter => e.bg,
                Colorization::BGAfter => e.bg,
            };
            last_colors = Colors { fg: e.fg, bg: e.bg };
        }
        let mut result = String::new();
        for e in &self.elements {
            result.push_str(colorize(&e.string, e.fg, e.bg).to_string().as_str());
        }

        Prompt {
            string: result,
            length,
        }
    }
}

#[cfg(test)]
mod tests {

    use super::config::{ContentType, PromptElement};

    use super::Colorization;

    use super::EnvData;
    use super::GitData;

    use super::PromptString;

    use super::Color;
    use super::Prompt;

    // This is a replication of the method with same name in main.rs.
    // I need some more refactoring. This should be a struct method for
    // not yet existing struct PromptSpec. These test could be moved then.
    fn factor_prompt_string(
        prompt_spec: &Vec<PromptElement>,
        git_data: &Option<GitData>,
        env_data: &EnvData,
    ) -> Prompt {
        let mut prompt = PromptString::factor_from_spec(prompt_spec, true, git_data, env_data);
        prompt.get_promt()
    }

    fn _factor_const_element(value: String, fg: Colorization, bg: Colorization) -> PromptElement {
        PromptElement {
            bg,
            fg,
            post: String::from(""),
            pre: String::from(""),
            skip_all_when_zero: false,
            skip_value_when_zero: false,
            skip_value_when_one: false,
            content: ContentType::Const(value.to_string()),
        }
    }

    fn _factor_delimiter_element(
        value: String,
        fg: Colorization,
        bg: Colorization,
    ) -> PromptElement {
        PromptElement {
            bg,
            fg,
            post: String::from(""),
            pre: String::from(""),
            skip_all_when_zero: false,
            skip_value_when_zero: false,
            skip_value_when_one: false,
            content: ContentType::Delimiter(value.to_string()),
        }
    }

    fn _factor_scope_element(
        start: bool,
        value: String,
        fg: Colorization,
        bg: Colorization,
    ) -> PromptElement {
        PromptElement {
            bg,
            fg,
            post: String::from(""),
            pre: String::from(""),
            skip_all_when_zero: false,
            skip_value_when_zero: false,
            skip_value_when_one: false,
            content: if start {
                ContentType::ScopeStart(value.to_string())
            } else {
                ContentType::ScopeEnd(value.to_string())
            },
        }
    }

    #[test]
    fn one_element_default() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 1);
        assert_eq!(output.string, "[×@×:a]".to_string());
    }

    #[test]
    fn one_element_specific() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Specific(Color::White),
            Colorization::Specific(Color::Black),
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 1);
        assert_eq!(output.string, "[White@Black:a]".to_string());
    }

    #[test]
    fn element_with_fgbefore_as_fg() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Specific(Color::White),
            Colorization::Specific(Color::Black),
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::FGBefore,
            Colorization::Specific(Color::Red),
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 2);
        assert_eq!(output.string, "[White@Black:a][White@Red:b]".to_string());
    }

    #[test]
    fn element_with_fgbefore_as_bg() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Specific(Color::White),
            Colorization::Specific(Color::Black),
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::Specific(Color::Red),
            Colorization::FGBefore,
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 2);
        assert_eq!(output.string, "[White@Black:a][Red@White:b]".to_string());
    }

    #[test]
    fn element_with_bgbefore_as_fg() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Specific(Color::White),
            Colorization::Specific(Color::Black),
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::BGBefore,
            Colorization::Specific(Color::Red),
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 2);
        assert_eq!(output.string, "[White@Black:a][Black@Red:b]".to_string());
    }

    #[test]
    fn element_with_bgbefore_as_bg() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Specific(Color::White),
            Colorization::Specific(Color::Black),
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::Specific(Color::Red),
            Colorization::BGBefore,
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 2);
        assert_eq!(output.string, "[White@Black:a][Red@Black:b]".to_string());
    }

    #[test]
    fn fgbefore_and_bgbefore_in_1st_element_give_default_color() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::FGBefore,
            Colorization::BGBefore,
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 1);
        assert_eq!(output.string, "[×@×:a]".to_string());
    }

    #[test]
    fn element_with_fgafter_as_fg() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::FGAfter,
            Colorization::Specific(Color::Red),
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::Specific(Color::White),
            Colorization::Specific(Color::Black),
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 2);
        assert_eq!(output.string, "[White@Red:a][White@Black:b]".to_string());
    }

    #[test]
    fn element_with_fgafter_as_bg() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Specific(Color::Red),
            Colorization::FGAfter,
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::Specific(Color::White),
            Colorization::Specific(Color::Black),
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 2);
        assert_eq!(output.string, "[Red@White:a][White@Black:b]".to_string());
    }

    #[test]
    fn element_with_bgafter_as_fg() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::BGAfter,
            Colorization::Specific(Color::Red),
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::Specific(Color::White),
            Colorization::Specific(Color::Black),
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 2);
        assert_eq!(output.string, "[Black@Red:a][White@Black:b]".to_string());
    }

    #[test]
    fn element_with_bgafter_as_bg() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Specific(Color::Red),
            Colorization::BGAfter,
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::Specific(Color::White),
            Colorization::Specific(Color::Black),
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 2);
        assert_eq!(output.string, "[Red@Black:a][White@Black:b]".to_string());
    }

    #[test]
    fn three_elements_with_delimiter() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_delimiter_element(
            "D".to_string(),
            Colorization::Specific(Color::Green),
            Colorization::Specific(Color::Black),
        ));
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Specific(Color::Red),
            Colorization::Specific(Color::Black),
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::Specific(Color::Blue),
            Colorization::Specific(Color::White),
        ));
        given.push(_factor_const_element(
            "c".to_string(),
            Colorization::Specific(Color::Green),
            Colorization::Specific(Color::Magenta),
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 5);
        assert_eq!(
            output.string,
            "[Red@Black:a][Green@Black:D][Blue@White:b][Green@Black:D][Green@Magenta:c]"
                .to_string()
        );
    }

    #[test]
    fn delimiter_stops() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_delimiter_element(
            "D".to_string(),
            Colorization::Specific(Color::Green),
            Colorization::Specific(Color::Black),
        ));
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Specific(Color::Red),
            Colorization::Specific(Color::Black),
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::Specific(Color::Blue),
            Colorization::Specific(Color::White),
        ));
        given.push(_factor_delimiter_element(
            "".to_string(),
            Colorization::Specific(Color::Green),
            Colorization::Specific(Color::Black),
        ));
        given.push(_factor_const_element(
            "c".to_string(),
            Colorization::Specific(Color::Green),
            Colorization::Specific(Color::Magenta),
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 4);
        assert_eq!(
            output.string,
            "[Red@Black:a][Green@Black:D][Blue@White:b][Green@Magenta:c]".to_string()
        );
    }

    #[test]
    fn delimiter_redefined() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_delimiter_element(
            "D".to_string(),
            Colorization::Specific(Color::Green),
            Colorization::Specific(Color::Black),
        ));
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Specific(Color::Red),
            Colorization::Specific(Color::Black),
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::Specific(Color::Blue),
            Colorization::Specific(Color::White),
        ));
        given.push(_factor_delimiter_element(
            "DIM".to_string(),
            Colorization::Specific(Color::Green),
            Colorization::Specific(Color::Black),
        ));
        given.push(_factor_const_element(
            "c".to_string(),
            Colorization::Specific(Color::Green),
            Colorization::Specific(Color::Magenta),
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 7);
        assert_eq!(
            output.string,
            "[Red@Black:a][Green@Black:D][Blue@White:b][Green@Black:DIM][Green@Magenta:c]"
                .to_string()
        );
    }

    #[test]
    fn delimiter_with_relative_colors() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_delimiter_element(
            "D".to_string(),
            Colorization::BGBefore,
            Colorization::BGAfter,
        ));
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Specific(Color::Red),
            Colorization::Specific(Color::Black),
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::Specific(Color::Blue),
            Colorization::Specific(Color::White),
        ));
        given.push(_factor_const_element(
            "c".to_string(),
            Colorization::Specific(Color::Green),
            Colorization::Specific(Color::Magenta),
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 5);
        assert_eq!(
            output.string,
            "[Red@Black:a][Black@White:D][Blue@White:b][White@Magenta:D][Green@Magenta:c]"
                .to_string()
        );
    }

    #[test]
    fn one_scope_at_start_and_end_with_content() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_scope_element(
            true,
            "<".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            ">".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 3);
        assert_eq!(output.string, "[×@×:<][×@×:a][×@×:>]".to_string());
    }

    #[test]
    fn one_scope_at_start_and_end_without_content() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_scope_element(
            true,
            "<".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            ">".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 0);
        assert_eq!(output.string, "".to_string());
    }

    #[test]
    fn two_scopes_with_content() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_scope_element(
            true,
            "<".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            true,
            "((".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            "))".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            ">".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 8);
        assert_eq!(
            output.string,
            "[×@×:<][×@×:a][×@×:((][×@×:b][×@×:))][×@×:>]".to_string()
        );
    }

    #[test]
    fn two_scopes_without_content() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_scope_element(
            true,
            "<".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            true,
            "((".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            "))".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            ">".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 0);
        assert_eq!(output.string, "".to_string());
    }

    #[test]
    fn two_scopes_with_content_only_in_outer() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_scope_element(
            true,
            "<".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            true,
            "((".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            "))".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            ">".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 3);
        assert_eq!(output.string, "[×@×:<][×@×:a][×@×:>]".to_string());
    }

    #[test]
    fn two_scopes_with_content_only_in_inner() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_scope_element(
            true,
            "<".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            true,
            "((".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            "))".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            ">".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 7);
        assert_eq!(
            output.string,
            "[×@×:<][×@×:((][×@×:a][×@×:))][×@×:>]".to_string()
        );
    }

    #[test]
    fn two_subsequent_scopes_with_content() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_scope_element(
            true,
            "<".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            ">".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            true,
            "→".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            "←".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.length, 6);
        assert_eq!(
            output.string,
            "[×@×:<][×@×:a][×@×:>][×@×:→][×@×:b][×@×:←]".to_string()
        );
    }

    #[test]
    fn no_delimiters_applied_on_scope_elements() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_delimiter_element(
            "|".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            true,
            "<".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "c".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "d".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            ">".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "e".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "f".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.string, "[×@×:a][×@×:|][×@×:b][×@×:|][×@×:<][×@×:c][×@×:|][×@×:d][×@×:>][×@×:|][×@×:e][×@×:|][×@×:f]".to_string());
        assert_eq!(output.length, 13);
    }

    #[test]
    fn no_delimiters_applied_on_scope_elements_with_multiple_scopes() {
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_delimiter_element(
            "|".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            true,
            "<".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            true,
            "(".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "c".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "d".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            ")".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            ">".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "e".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "f".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(output.string, "[×@×:a][×@×:|][×@×:b][×@×:|][×@×:<][×@×:(][×@×:c][×@×:|][×@×:d][×@×:)][×@×:>][×@×:|][×@×:e][×@×:|][×@×:f]".to_string());
        assert_eq!(output.length, 15);
    }

    #[test]
    fn scopes_with_content_and_delimiter_defined_ahead() {
        // “delimiter skip” bug
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_delimiter_element(
            "-".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            true,
            "<".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "b".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "c".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            ">".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(
            output.string,
            "[×@×:<][×@×:a][×@×:-][×@×:b][×@×:-][×@×:c][×@×:>]".to_string()
        );
        assert_eq!(output.length, 7);
    }

    #[test]
    fn delimiter_defined_within_scope_has_no_influence_on_delimiter_printed_before_scope_opening_element(
    ) {
        // reprocuce bug: if delimiter is specified within scope but before first non-empty
        // element, it’s used as delimiter *before* the scope opening element.
        let mut given: Vec<PromptElement> = Vec::new();
        given.push(_factor_delimiter_element(
            "-".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "a".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            true,
            "<".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_delimiter_element(
            "!".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "c".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_const_element(
            "d".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        given.push(_factor_scope_element(
            false,
            ">".to_string(),
            Colorization::Default,
            Colorization::Default,
        ));
        let output = factor_prompt_string(&given, &None, &EnvData {});
        assert_eq!(
            output.string,
            "[×@×:a][×@×:-][×@×:<][×@×:c][×@×:!][×@×:d][×@×:>]".to_string()
        );
        assert_eq!(output.length, 7);
    }
}
