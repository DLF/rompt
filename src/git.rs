use git2::BranchType;
use git2::Error;
use git2::Repository;
use git2::Status;
use git2::StatusOptions;
use git2::Statuses;

pub struct GitStatusData {
    pub wt_new: i32,
    pub wt_modified: i32,
    pub wt_deleted: i32,
    pub wt_typechange: i32,
    pub wt_renamed: i32,
    pub ix_new: i32,
    pub ix_modified: i32,
    pub ix_deleted: i32,
    pub ix_typechange: i32,
    pub ix_renamed: i32,
    pub current: i32,
    pub ignored: i32,
    pub conflicted: i32,
    pub all: i32,
}

pub struct GitBranchData {
    pub branch_name: Option<String>,
    pub ahead_behind: Option<(usize, usize)>,
}

pub struct GitData {
    pub status_data: GitStatusData,
    pub branch_data: GitBranchData,
}

impl GitData {
    pub fn new() -> Option<GitData> {
        match Repository::discover(".") {
            Ok(repo) => {
                match repo.statuses(Some(&mut StatusOptions::new().include_untracked(true))) {
                    Ok(statuses) => Some(GitData {
                        status_data: git_status_data(statuses),
                        branch_data: git_branch_data(&repo),
                    }),
                    Err(_) => None,
                }
            }
            Err(_) => None,
        }
    }
}

fn git_status_data(statuses: Statuses) -> GitStatusData {
    let mut result = GitStatusData {
        wt_new: 0,
        wt_modified: 0,
        wt_deleted: 0,
        wt_typechange: 0,
        wt_renamed: 0,
        ix_new: 0,
        ix_modified: 0,
        ix_deleted: 0,
        ix_typechange: 0,
        ix_renamed: 0,
        current: 0,
        ignored: 0,
        conflicted: 0,
        all: 0,
    };
    for status_entry in statuses.iter() {
        let status = status_entry.status();
        if status.contains(Status::WT_NEW) {
            // modified in local repo
            result.wt_new += 1;
        }
        if status.contains(Status::WT_MODIFIED) {
            // modified in local repo
            result.wt_modified += 1;
        }
        if status.contains(Status::WT_DELETED) {
            result.wt_deleted += 1;
        }
        if status.contains(Status::WT_TYPECHANGE) {
            result.wt_typechange += 1;
        }
        if status.contains(Status::WT_RENAMED) {
            result.wt_renamed += 1;
        }
        if status.contains(Status::INDEX_NEW) {
            // added new file in staging area
            result.ix_new += 1;
        }
        if status.contains(Status::INDEX_MODIFIED) {
            // added changed file in staging area
            result.ix_modified += 1;
        }
        if status.contains(Status::INDEX_DELETED) {
            result.ix_deleted += 1;
        }
        if status.contains(Status::INDEX_TYPECHANGE) {
            result.ix_typechange += 1;
        }
        if status.contains(Status::INDEX_RENAMED) {
            result.ix_renamed += 1;
        }
        if status.contains(Status::CURRENT) {
            result.current += 1;
        }
        if status.contains(Status::IGNORED) {
            result.ignored += 1;
        }
        if status.contains(Status::CONFLICTED) {
            result.conflicted += 1;
        }
        result.all += 1;
        //println!("{:#018b}", status.bits());
    }
    result
}

fn git_branch_data(repo: &Repository) -> GitBranchData {
    fn _git_branch_data(repo: &Repository) -> Result<GitBranchData, Error> {
        if repo.is_empty().unwrap() {
            Ok(GitBranchData {
                branch_name: Some("BigBang".to_string()),
                ahead_behind: None,
            })
        } else {
            let head_ref = repo.head()?;
            let branch_name = match head_ref.is_branch() {
                true => head_ref.shorthand().unwrap().to_string(),
                false => head_ref
                    .target()
                    .unwrap()
                    .to_string()
                    .chars()
                    .take(7)
                    .collect(),
            };
            let ahead_behind =
                match repo.find_branch(head_ref.shorthand().unwrap(), BranchType::Local) {
                    Ok(local) => match local.upstream() {
                        Ok(remote) => match local.get().target() {
                            Some(local_oid) => match remote.get().target() {
                                Some(remote_oid) => {
                                    match repo.graph_ahead_behind(local_oid, remote_oid) {
                                        Ok(ab) => Some(ab),
                                        Err(_) => None,
                                    }
                                }
                                None => None,
                            },
                            None => None,
                        },
                        Err(_) => None,
                    },
                    Err(_) => None,
                };
            Ok(GitBranchData {
                branch_name: Some(branch_name.to_string()),
                ahead_behind: ahead_behind,
            })
        }
    }

    match _git_branch_data(repo) {
        Ok(gbd) => gbd,
        Err(_) => GitBranchData {
            branch_name: None,
            ahead_behind: None,
        },
    }
}
