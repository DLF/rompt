use regex::Regex;
use term_size;

extern crate clap;
use clap::{App, Arg};

pub mod config;
pub mod env;
pub mod git;
pub mod promptstring;

use env::EnvData;
use git::GitData;
use promptstring::Prompt;
use promptstring::PromptString;

fn padded_prompt(prompt: Prompt) -> String {
    let (columns, _h) = term_size::dimensions().unwrap();
    let indent = (columns as usize) - prompt.length;
    let mut result = " ".repeat(indent);
    result.push_str(&prompt.string);
    result
}

/// Creates the complete prompt string, ready to print.
fn factor_output(config: &config::Config, debug_output: bool) -> String {
    let git_data = GitData::new();
    let env_data = EnvData::new();
    let pre_left = match &config.pre_left {
        Some(v) => {
            Some(PromptString::factor_from_spec(&v, debug_output, &git_data, &env_data).get_promt())
        }
        None => None,
    };
    let left = match &config.left {
        Some(v) => {
            Some(PromptString::factor_from_spec(&v, debug_output, &git_data, &env_data).get_promt())
        }
        None => None,
    };
    let right = match &config.right {
        Some(v) => {
            Some(PromptString::factor_from_spec(&v, debug_output, &git_data, &env_data).get_promt())
        }
        None => None,
    };
    format!(
        "{}{}",
        match pre_left {
            Some(v) => {
                if debug_output {
                    println!("Pre-Left:");
                };
                v.string + "\n"
            }
            None => String::from(""),
        },
        if config.right == None {
            match left {
                Some(v) => {
                    if debug_output {
                        println!("Left (Right == None):");
                    };
                    v.string
                }
                None => String::from(""),
            }
        } else {
            format!(
                "{}\r{}",
                match right {
                    Some(v) => {
                        if debug_output {
                            println!("Right:");
                        };
                        padded_prompt(v)
                    }
                    None => String::from(""),
                },
                match left {
                    Some(v) => {
                        if debug_output {
                            println!("Left:");
                        };
                        v.string
                    }
                    None => String::from(""),
                }
            )
        }
    )
}

fn main() {
    let args = App::new("rompt")
        .version("0.0.1")
        .author("dlf")
        .about("a terminal prompt")
        .arg(
            Arg::with_name("no_escape")
                .short("e")
                .long("no-escape")
                .help("disable escaping to use rompt directly"),
        )
        .arg(
            Arg::with_name("debug")
                .short("d")
                .long("debug")
                .help("enable debug output"),
        )
        .get_matches();

    colored::control::set_override(true);
    let config = config::get_config();
    let prompt_string = factor_output(&config, args.is_present("debug"));
    println!(
        "{}",
        match args.is_present("no_escape") {
            true => prompt_string,
            false => {
                let shell_format_command = Regex::new(r"\u{1b}(?P<c>[^m]*m)").unwrap();
                shell_format_command
                    .replace_all(&prompt_string, "\\[\\e$c\\]")
                    .to_string()
            }
        }
    );
}

//fn bytes_string(s: &String) -> String {
//    let a = s.as_bytes().into_iter().map(|x: &u8| -> String{ x.to_string()});
//    let b = s.as_bytes().into_iter().map(|x: &u8| -> String{
//        if *x > 32 {format!("{}",char::from(*x))} else {x.to_string()}
//    });
//    a.collect::<Vec<String>>().join("-") + "\n" + &b.collect::<Vec<String>>().join("-")
//}
