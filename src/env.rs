use hostname;
use std::env;

pub struct EnvData {}

impl EnvData {
    pub fn new() -> EnvData {
        EnvData {}
    }

    pub fn get_env_variable(&self, name: String) -> Option<String> {
        match env::var(name) {
            Ok(val) => Some(val),
            Err(_) => None,
        }
    }

    pub fn get_py_virt_env(&self) -> Option<String> {
        match self.get_env_variable("VIRTUAL_ENV".to_string()) {
            Some(val) => Some(
                val.split("/")
                    .collect::<Vec<_>>()
                    .last()
                    .unwrap()
                    .to_string(),
            ),
            None => None,
        }
    }

    pub fn get_user(&self) -> Option<String> {
        match self.get_env_variable("USER".to_string()) {
            Some(u) => Some(u),
            None => self.get_env_variable("USERNAME".to_string()),
        }
    }

    pub fn get_host(&self) -> Option<String> {
        match hostname::get() {
            Ok(v) => match v.into_string() {
                Ok(w) => Some(w),
                Err(_) => None,
            },
            Err(_) => None,
        }
    }

    pub fn get_ssh_client(&self) -> Option<String> {
        match self.get_env_variable("SSH_CLIENT".to_string()) {
            Some(val) => Some(
                val.split(" ")
                    .collect::<Vec<_>>()
                    .first()
                    .unwrap()
                    .to_string(),
            ),
            None => None,
        }
    }
}
