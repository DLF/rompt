[![pipeline status](https://gitlab.com/DLF/rompt/badges/master/pipeline.svg)](https://gitlab.com/DLF/rompt/-/commits/master)

A configurable shell prompt, fast and flexible.

Early development state.

# Alternatives
There are a lot of solutions for fancy shell prompts. Here are some I stumbled across.

### [powerline-shell](https://github.com/b-ryan/powerline-shell)
A very popular Python based prompt program. Supports a lot of data elements. Downside:
It’s really slow, there is a significant delay each time the prompt is printed.

### [powerline-rust](https://github.com/cirho/powerline-rust)
### [silver](https://github.com/reujab/silver)
### [bronze](https://github.com/reujab/bronze)

### [oh-my-bash](https://github.com/ohmybash/oh-my-bash/)
### [Agnoster ZSH](https://github.com/agnoster/agnoster-zsh-theme)

