# ToDos
* ✓ provide delim-element
* ✓ Use a color wrapper struct Default, Some(Color), BG_Before, BG_After, FG_Before, FG_After
* ✓ Bug: typing random long stuff causes bash to carriage-return...
  - happens with kitty, alacritty, and xterm
  - happens in ~ as well as in a git repo
  - does NOT happen with prompt "minimal"
  - does NOT happen with prompt "colorless"
  - ⇒ It seems that the number of chars of the color instructions is summing up to the space from the right when the line
    breaks. If this is 0 (like it should be), also the newline is working fine.
  - Check https://itectec.com/superuser/linux-coloring-bash-prompt-will-break-carriage-return-2/
* ✓ disable backslash escaping via argument
* ✓ Provide any ref as branch name if head is not at a branch
* ✓ section-start and section-close elements which show their const values only if the enclosed elements are not all empy
* ✓ allow pre-line left
* ✓ element: general env variable
* ✓ element: user
* ✓ element: host
* ✓ element: ssh
* ✓ CI for unittests (https://chrismacnaughton.com/blog/rust/rust-gitlab-ci/)
* rework color and delimiter logic:
  * if no fg/bg color is specified, use the last one, not "Default"
  * provide a "set color" element (without own output)
  * use "current delimiter" per scope
    * put delimiters on a stack, managed in "apply"
  * Think about scope and colors: Does it make sense to also use the "last color" per scope?
* element: working dir (with special separator and color sequences; will be tricky in context of next point)
* prefix substitution for values of all kinds of elements (add a map prefix->sub in the config)
* Fix: Git: deletions are not shown as change
* support nunbered colors
* support rgb colors
* fetch version from Cargo.toml (?)
* allow specifying config by argument
* provide AUR package (and maybe others; see https://skerritt.blog/packaging-your-rust-package/)
* Provide Conditionals (for sections?):
  * Git head types
  * Git dirty?
  * am I root? or better: who-am-I?
  * SSH?
  * env var exist
  * env var value is x
* allow pre-line center, and right prompts with fillers
* Git: Provide the tag name if a non-branch ref is checked out.
  * https://stackoverflow.com/questions/19808208/whats-the-fastest-way-to-find-tags-pointing-to-commits
* Git: Provide head type as additional struct element
* general sub-process element
* provide user and host (get independent from bash)
* only fetch data that is really needed for the current configuration
* Bug: find the reason why the prompt input breaks just before the last character when using a right prompt and fix it if possible
  (It seems that there is one control character not yet escaped. A quick test has shown that the \r cannot be escaped like the ANSI stuff.)
* make config serializable and use a cache for the config

